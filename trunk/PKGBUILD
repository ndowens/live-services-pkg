# Maintainer: artoo <artoo@artixlinux.org>
# Contributor: nous <nous@artixlinux.org>
# Contributor: dudemanguy <dudemanguy@artixlinux.org>

pkgbase=live-services
pkgname=('artix-live-base'
        'artix-live-openrc'
        'artix-live-runit'
        'artix-live-s6'
        'artix-live-66')
pkgver=0.10
pkgrel=1
pkgdesc='Artix live session'
arch=('any')
url="https://gitea.artixlinux.org/artix/live-services"
license=('GPL')
makedepends=('git')
# Build a commit point
# _commit=e9e2ec9d9e57b4d4eeb5a329314f888a3791085a # tags/0.10
# source=("git+$url.git#commit=$_commit")
source=("${pkgbase}-${pkgver}.tar.gz::$url/archive/${pkgver}.tar.gz")
sha256sums=('8f93bd8087c6f4ece2d3e9c68fdb4010561b41647825ebc381de37c322348504')

build() {
    cd ${pkgbase}
    make
}

package_artix-live-base() {
    pkgdesc='Artix live base scripts'
    depends=('artools-base')

    cd ${pkgbase}
    make DESTDIR=${pkgdir} install_base install_xdg
}

package_artix-live-openrc() {
    pkgdesc='Artix live openrc init scripts'
    depends=('openrc' 'artix-live-base')

    cd ${pkgbase}
    make DESTDIR=${pkgdir} install_rc

    install -d "${pkgdir}"/etc/runlevels/default
    ln -sf /etc/init.d/pacman-init "${pkgdir}"/etc/runlevels/default/pacman-init
    ln -sf /etc/init.d/artix-live "${pkgdir}"/etc/runlevels/default/artix-live
}

package_artix-live-runit() {
    pkgdesc='Artix live runit init scripts'
    depends=('runit' 'artix-live-base')

    cd ${pkgbase}
    make DESTDIR=${pkgdir} install_runit

    install -d "${pkgdir}"/etc/runit/runsvdir/default
    ln -sf /etc/runit/sv/pacman-init "${pkgdir}"/etc/runit/runsvdir/default/pacman-init
}

package_artix-live-s6() {
    pkgdesc='Artix live s6 init scripts'
    depends=('s6' 'artix-live-base')
    install=live-s6.install

    cd ${pkgbase}
    make DESTDIR=${pkgdir} install_s6
}

package_artix-live-66() {
    pkgdesc='Artix live 66 init scripts'
    depends=('66' 'artix-live-base' '66-scripts')
    install=live-66.install

    cd ${pkgbase}
    make DESTDIR=${pkgdir} install_66
}
